/**
******************************************************************************
* @file    ATMEL_TWI_EEPROM.c
* @author  Majid Derhambakhsh
* @version V1.6.1
* @date    09-11-2018
* @brief
******************************************************************************
* @description  Set up and use the AT24Cxxxx IC's
*   
* @attention    This file is for Codevision AVR 
                AEEPROMSR [-|-|-|-|DataRead|DataErase|DataWrite|Connected]
                Atmel eeprom Status Register:
                 -DataRead:This flag bit is set when Data is read
                 -DataErase:This flag bit is set when Data is erase
                 -DataWrite:This flag bit is set when Data is write
                 -Connected:This flag bit is set when IC connected 
******************************************************************************
*/

/******************** Librarys Include ********************/ 
#include "ATMEL_TWI_EEPROM.h"
/*-----------------------------------------------*/
#include <io.h>
#include <delay.h>
/*-----------------------------------------------*/
#include "Hardware_unit.h"
/*--------------- Create Variable ---------------*/
uint8_t AEEPROMSR=0x00; /* Variable for register */
static uint8_t high_byte_address=0; /* Variable for set the addres */
static uint8_t low_byte_address=0; /* Variable for set the addres */
static uint8_t connection_test=0; /* Variable for connection check */
static uint16_t new_address=0;               /* Variable for set up addres */
static uint16_t start_page=0;               /* Variable for set up start page */
static uint16_t end_page=0;                 /* Variable for set up end page */
static uint16_t current_page=0;             /* Variable for set up current page */
static uint32_t remaining_amount_of_page=0; /* Variable for get the remainder of the page */
static uint8_t data_copy_counter=0;         /* Variable for data copy counter */
static uint8_t data_copy=0;                 /* Variable for copy datas */
/**** ***************** Functions **********************/
void atmel_eeprom_init(void) /* Function for initiation twi */
{
 TWSR=0x00; /* Set twi prescaler*/
 TWBR=0x20; /* Set the twi clock frequency equal to 100KHz */
 TWAR=0x64; /* Set the device address (64H-100D) */
 TWCR=0x04; /* Enable twi */
 WP_CONFIG_PORT=(1<<WP_PIN); /* Set write protect pin as output */
 ENABLE_GLOBAL_INTERRUPT; /* Global Interrupt Enable */
}
/*-----------------------------------------------*/
uint8_t atmel_eeprom_connection_test(void) /* Connetction test */
{
 AEEPROMSR=RESET_FLAG;    /* Reset all flags */
 TWCR=0xA4;               /* Create Start Conditions */
 while(!(TWCR&0x80));     /* Waiting For Twint */
 if((TWSR&0xF8)==0x08)    /* A START Condition Has Been Transmitted */
 {
  TWDR=0xA0;              /* Set The IC Address As Receiver */
  TWCR=0x84;              /* Send TWDR */
 }  
 else                     /* If connection lost trying to reconnect */
 {
  TWCR=0xA4;               /* Create Restart Mode */
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x10)    /* A Repeated START Condition Has Been Transmitted */
  {
   TWDR=0xA0;              /* Set The IC Address As Receiver */
   TWCR=0x84;              /* Send TWDR */
  }
 } 
 while(!(TWCR&0x80));     /* Waiting For Twint */
 if((TWSR&0xF8)==0x18)    /* SLA+W Has Been Transmitted & ACK Has Been Received */
 {
  TWCR=0x94;              /* Send TWDR */
  delay_ms(WAIT_FOR_END_SELF_TIMED_WRITE_CYCLE);            /* Delay for next connection */
  return TRUE;            /* IF The connection was established send true and we cut the connection */
  AEEPROMSR|=CONNECTED;   /* connection bit set */
 }
 else
 {
  TWCR=0x94;              /* Send TWDR */ 
  delay_ms(WAIT_FOR_END_SELF_TIMED_WRITE_CYCLE);            /* Delay for next connection */
  return FALSE;           /* IF The connection was not established send false and we cut the connection */
  AEEPROMSR=RESET_FLAG;   /* connection bit reset */
 } 
}
/*-----------------------------------------------*/
void atmel_eeprom_write_byte(uint16_t wb_addres,uint8_t wb_eeprom_data) /* Function For Send Data */
{ 
 connection_test=FALSE; /* Set the value to FALSE */
 connection_test=atmel_eeprom_connection_test(); /* Connection check */
 if(connection_test==TRUE)                       /* Connection ok */
 {
  AEEPROMSR=RESET_FLAG; /* Reset all flags */
  high_byte_address=((wb_addres >> 8) & 0xFF); /* Set up High Byte Addres */
  low_byte_address=(wb_addres & 0xFF);         /* Set up low Byte Addres */
 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/ 
  WP_PORT=(0<<WP_PIN);     /* Disable write protect */
  TWCR=0xA4;               /* Create Start Conditions */
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x08)    /* A START Condition Has Been Transmitted */
  {
   TWDR=0xA0;              /* Set The IC Address As Receiver */
   TWCR=0x84;              /* Send TWDR */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x18)    /* SLA+W Has Been Transmitted & ACK Has Been Received */
  {
   TWDR=high_byte_address;  /* Set High Byte Addres In IC */
   TWCR=0x84;              /* Send TWDR */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x28)    /* High Byte Has Been Transmitted & ACK Has Been Received */
  {
   TWDR=low_byte_address;   /* Set Low Byte Addres In IC */
   TWCR=0x84;              /* Send TWDR */
  } 
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x28)    /* Low Byte Has Been Transmitted & ACK Has Been Received */
  {
   TWDR=wb_eeprom_data;       /* Copy The Input Data In The Buffer */
   TWCR=0x84;              /* Send TWDR */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x28)    /* Data Byte Has Been Transmitted & ACK Has Been Received */
  {
   TWCR=0x94;              /* Create A Stopping Condition */
  }
  delay_ms(WAIT_FOR_END_SELF_TIMED_WRITE_CYCLE);
  WP_PORT=(1<<WP_PIN);     /* Enable write protect */
  AEEPROMSR|=DATA_WRITE;   /* Data write flag bit set */
 }
 else{AEEPROMSR=RESET_FLAG; /* Reset all flags */}
}
/*-----------------------------------------------*/
void atmel_eeprom_write_nbyte(uint8_t *wnb_str,uint16_t wnb_start_byte,uint16_t wnb_nbyte) /* Function For Send Data */
{ 
/*^^^^^^^^^^^^^^^ Set Variable value ^^^^^^^^^^^^^^^^*/
 high_byte_address=0;        /* Set default value */
 low_byte_address=0;         /* Set default value */
 connection_test=0;          /* Set default value */
 new_address=0;              /* Set default value */              
 start_page=0;               /* Set default value */              
 end_page=0;                 /* Set default value */                
 current_page=0;             /* Set default value */            
 remaining_amount_of_page=0; /* Set default value */
 data_copy_counter=0;        /* Set default value */         
 data_copy=0;                /* Set default value */                 
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
 connection_test=FALSE; /* Set the value to FALSE */
 connection_test=atmel_eeprom_connection_test(); /* Connection check */
 if(connection_test==TRUE)                       /* Connection ok */
 { 
  AEEPROMSR=RESET_FLAG; /* Reset all flags */
  start_page=((wnb_start_byte+PAGE_BYTES)/PAGE_BYTES); /* Start page to write */
  end_page=((wnb_start_byte+wnb_nbyte+PAGE_BYTES)/PAGE_BYTES)+NEXT_PAGE_SELECT; /* Number of pages required to write */
 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/  
  for(;start_page<end_page;start_page++) /* Page selector loop */
  {  
   new_address+=(wnb_start_byte+data_copy_counter); /* Set the new addres for write */
   current_page=((wnb_start_byte+PAGE_BYTES)/PAGE_BYTES);
   remaining_amount_of_page=(PAGE_BYTES*(uint32_t)current_page)-(uint32_t)wnb_start_byte; /* Get remaining amount of page for write control */
   if(remaining_amount_of_page<wnb_nbyte) /* Test remaining amount of page for write control */ 
   {
    data_copy_counter=remaining_amount_of_page; /* Initialize to write on page */
   }
   else{data_copy_counter=wnb_nbyte;} /* Initialize to write on page */
  /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/ 
   high_byte_address=((new_address >> 8) & 0xFF); /* Set up High Byte Addres */
   low_byte_address=(new_address & 0xFF);         /* Set up low Byte Addres */
  /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/ 
   WP_PORT=(0<<WP_PIN);     /* Disable write protect */
   TWCR=0xA4;               /* Create Start Conditions */
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x08)    /* A START Condition Has Been Transmitted */
   {
    TWDR=0xA0;              /* Set The IC Address As Receiver */
    TWCR=0x84;              /* Send TWDR */
   }
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x18)    /* SLA+W Has Been Transmitted & ACK Has Been Received */
   {
    TWDR=high_byte_address;  /* Set High Byte Addres In IC */
    TWCR=0x84;              /* Send TWDR */
   }
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x28)    /* High Byte Has Been Transmitted & ACK Has Been Received */
   {
    TWDR=low_byte_address;   /* Set Low Byte Addres In IC */
    TWCR=0x84;              /* Send TWDR */
   } 
   for(data_copy=0;data_copy<data_copy_counter;data_copy++) /* Control loop to write on the page */
   {
    while(!(TWCR&0x80));     /* Waiting For Twint */
    if((TWSR&0xF8)==0x28)    /* Low Byte Has Been Transmitted & ACK Has Been Received */
    {
     TWDR=*wnb_str;         /* Copy The Input Data In The Buffer */
     TWCR=0x84;             /* Send TWDR */
     wnb_str++;             /* Select next byte */
    }
   }                                                        /* End control loop */
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x28)    /* Data Byte Has Been Transmitted & ACK Has Been Received */
   {
    TWCR=0x94;              /* Create A Stopping Condition */
   } 
   wnb_nbyte=wnb_nbyte-data_copy_counter; /* Set the number of writing */
   wnb_start_byte=0;                  /* Reset to byte start */
   delay_ms(WAIT_FOR_END_SELF_TIMED_WRITE_CYCLE);                   /* Wait for end write */
   WP_PORT=(1<<WP_PIN);     /* Enable write protect */
  }
  AEEPROMSR|=DATA_WRITE; /* Data write flag bit set */
 }
 else{AEEPROMSR=RESET_FLAG; /* Reset all flags */} 
}
/*-----------------------------------------------*/
void atmel_eeprom_erase(uint16_t e_start_byte,uint16_t e_end_byte) /* Function For Erase EEPROM */
{
/*^^^^^^^^^^^^^^^^^ Create Variable ^^^^^^^^^^^^^^^^^*/
 uint32_t nbyte=0;                    /* Variable for number count */
/*^^^^^^^^^^^^^^^ Set Variable value ^^^^^^^^^^^^^^^^*/
 high_byte_address=0;        /* Set default value */
 low_byte_address=0;         /* Set default value */
 connection_test=0;          /* Set default value */
 new_address=0;              /* Set default value */              
 start_page=0;               /* Set default value */              
 end_page=0;                 /* Set default value */                
 current_page=0;             /* Set default value */            
 remaining_amount_of_page=0; /* Set default value */
 data_copy_counter=0;        /* Set default value */         
 data_copy=0;                /* Set default value */                 
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/ 
 connection_test=FALSE; /* Set the value to FALSE */
 connection_test=atmel_eeprom_connection_test(); /* Connection check */
 if(connection_test==TRUE)                       /* Connection ok */
 {
  AEEPROMSR=RESET_FLAG; /* Reset all flags */
  nbyte=((uint32_t)e_end_byte+CORRECT_THE_COUNT)-(uint32_t)e_start_byte;   /* set up number */
  start_page=((e_start_byte+PAGE_BYTES)/PAGE_BYTES); /* Start page to write */
  end_page=((e_start_byte+nbyte+PAGE_BYTES)/PAGE_BYTES)+NEXT_PAGE_SELECT; /* Number of pages required to write */
 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/  
  for(;start_page<end_page;start_page++) /* Page selector loop */
  {  
   new_address+=(e_start_byte+data_copy_counter); /* Set the new addres for write */
   current_page=((e_start_byte+PAGE_BYTES)/PAGE_BYTES);
   remaining_amount_of_page=(PAGE_BYTES*(uint32_t)current_page)-(uint32_t)e_start_byte; /* Get remaining amount of page for write control */
   if(remaining_amount_of_page<nbyte) /* Test remaining amount of page for write control */ 
   {
    data_copy_counter=remaining_amount_of_page; /* Initialize to write on page */
   }
   else{data_copy_counter=nbyte;} /* Initialize to write on page */
  /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/ 
   high_byte_address=((new_address >> 8) & 0xFF); /* Set up High Byte Addres */
   low_byte_address=(new_address & 0xFF);         /* Set up low Byte Addres */
  /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/ 
   WP_PORT=(0<<WP_PIN);     /* Disable write protect */
   TWCR=0xA4;               /* Create Start Conditions */
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x08)    /* A START Condition Has Been Transmitted */
   {
    TWDR=0xA0;              /* Set The IC Address As Receiver */
    TWCR=0x84;              /* Send TWDR */
   }
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x18)    /* SLA+W Has Been Transmitted & ACK Has Been Received */
   {
    TWDR=high_byte_address;  /* Set High Byte Addres In IC */
    TWCR=0x84;              /* Send TWDR */
   }
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x28)    /* High Byte Has Been Transmitted & ACK Has Been Received */
   {
    TWDR=low_byte_address;   /* Set Low Byte Addres In IC */
    TWCR=0x84;              /* Send TWDR */
   } 
   for(data_copy=0;data_copy<data_copy_counter;data_copy++) /* Control loop to write on the page */
   {
    while(!(TWCR&0x80));     /* Waiting For Twint */
    if((TWSR&0xF8)==0x28)    /* Low Byte Has Been Transmitted & ACK Has Been Received */
    {
     TWDR=ERASE;             /* Copy The Input Data In The Buffer */
     TWCR=0x84;              /* Send TWDR */
    }
   }                                                        /* End control loop */
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x28)    /* Data Byte Has Been Transmitted & ACK Has Been Received */
   {
    TWCR=0x94;              /* Create A Stopping Condition */
   }
   nbyte=nbyte-data_copy_counter; /* Set the number of writing */
   e_start_byte=0;                  /* Reset to byte start */
   delay_ms(WAIT_FOR_END_SELF_TIMED_WRITE_CYCLE);                   /* Wait for end write */
   WP_PORT=(1<<WP_PIN);     /* Enable write protect */
  }
  AEEPROMSR|=DATA_ERASE; /* Data erase flag bit set */
 }
 else{AEEPROMSR=RESET_FLAG; /* Reset all flags */}
}
/*-----------------------------------------------*/
uint8_t atmel_eeprom_read_byte(uint16_t rb_address) /* Function For Read EEPROM */
{
/*************** Create Variable ***************/  
 uint8_t data_save;
/************* End Create Variable *************/ 
 connection_test=FALSE; /* Set the value to FALSE */
 connection_test=atmel_eeprom_connection_test(); /* Connection check */
 if(connection_test==TRUE)                       /* Connection ok */
 {
  AEEPROMSR=RESET_FLAG; /* Reset all flags */
  high_byte_address=((rb_address >> 8) & 0xFF); /* Set up High Byte Addres */
  low_byte_address=(rb_address & 0xFF);         /* Set up low Byte Addres */
 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/  
  TWCR=0xA4;               /* Create Start Conditions */
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x08)    /* A START Condition Has Been Transmitted */
  {
   TWDR=0xA0;              /* Set The IC Address As Receiver */
   TWCR=0x84;              /* Send TWDR */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x18)    /* SLA+W Has Been Transmitted & ACK Has Been Received */
  {
   TWDR=high_byte_address;  /* Set High Byte Addres In IC */
   TWCR=0x84;              /* Send TWDR */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x28)    /* High Byte Has Been Transmitted & ACK Has Been Received */
  {
   TWDR=low_byte_address;   /* Set Low Byte Addres In IC */
   TWCR=0x84;              /* Send TWDR */
  } 
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x28)    /* Low Byte Has Been Transmitted & ACK Has Been Received */
  {
   TWCR=0xA4;              /* Create Restart Mode */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x10)    /* A Repeated START Condition Has Been Transmitted */
  {
   TWDR=0xA1;              /* Set The IC Address As Sender */
   TWCR=0x84;              /* Send TWDR */
  } 
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x40)    /* SLA+R Has Been Transmitted & ACK Has Been Received */
  {
   TWCR=0xC4;              /* Acknowledge Send */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x50)    /* Data Byte Has Been Received & ACK Has Been Sent */
  {
   data_save=TWDR;         /* Copy The Eeprom Data In The Variable */
   TWCR=0x84;              /* Send TWDR */
  } 
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x58)    /* Data Byte Has Been Received & NOT ACK Has Been Sent */
  {
   TWCR=0x94;              /* Create A Stopping Condition */
  }       
 /*************** Data Return ***************/
  delay_ms(WAIT_FOR_END_SELF_TIMED_WRITE_CYCLE);
  return data_save;        /* Return Eeprom Data */
  AEEPROMSR|=DATA_READ;    /* Data read flag bit set */
 }
 else{AEEPROMSR=RESET_FLAG; /* Reset all flags */}
}
/*-----------------------------------------------*/
void atmel_eeprom_read_nbyte(uint8_t *rnb_str,uint16_t rnb_start_byte,uint16_t rnb_nbyte) /* Function For Read EEPROM */
{
/*^^^^^^^^^^^^^^^^^ Create Variable ^^^^^^^^^^^^^^^^^*/
 uint16_t end_byte=0; /* Variable For Count */
/************* End Create Variable *************/ 
 connection_test=FALSE; /* Set the value to FALSE */
 connection_test=atmel_eeprom_connection_test(); /* Connection check */
 if(connection_test==TRUE)                       /* Connection ok */
 {
  AEEPROMSR=RESET_FLAG; /* Reset all flags */ 
  end_byte=(rnb_start_byte+rnb_nbyte); /* Set Value For Counter */
  high_byte_address=((rnb_start_byte >> 8) & 0xFF); /* Set up High Byte Addres */
  low_byte_address=(rnb_start_byte & 0xFF);         /* Set up low Byte Addres */
 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/   
  TWCR=0xA4;               /* Create Start Conditions */
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x08)    /* A START Condition Has Been Transmitted */
  {
   TWDR=0xA0;              /* Set The IC Address As Receiver */
   TWCR=0x84;              /* Send TWDR */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x18)    /* SLA+W Has Been Transmitted & ACK Has Been Received */
  {
   TWDR=high_byte_address;  /* Set High Byte Addres In IC */
   TWCR=0x84;              /* Send TWDR */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x28)    /* High Byte Has Been Transmitted & ACK Has Been Received */
  {
   TWDR=low_byte_address;   /* Set Low Byte Addres In IC */
   TWCR=0x84;              /* Send TWDR */
  } 
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x28)    /* High Byte Has Been Transmitted & ACK Has Been Received */
  {
   TWCR=0xA4;              /* Create Restart Mode */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x10)    /* A Repeated START Condition Has Been Transmitted */
  {
   TWDR=0xA1;              /* Set The IC Address As Sender */
   TWCR=0x84;              /* Send TWDR */
  } 
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x40)    /* SLA+R Has Been Transmitted & ACK Has Been Received */
  {
   TWCR=0xC4;              /* Acknowledge Send */
  }
  for(;rnb_start_byte<end_byte;rnb_start_byte++) /* Loop For Counts And Copy */
  {
   while(!(TWCR&0x80));     /* Waiting For Twint */
   if((TWSR&0xF8)==0x50)    /* Data Byte Has Been Received & ACK Has Been Sent */
   {
    *rnb_str=TWDR;              /* Copy The Eeprom Data In The Variable */
    TWCR=0xC4;              /* Send TWDR */
    rnb_str++;                  /* Select Next Byte */ 
   }
  }  
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x50)    /* Data Byte Has Been Received & ACK Has Been Sent */
  {
   TWCR=0x84;              /* Send TWDR */
  }
  while(!(TWCR&0x80));     /* Waiting For Twint */
  if((TWSR&0xF8)==0x58)    /* Data Byte Has Been Received & NOT ACK Has Been Sent */
  {
   TWCR=0x94;              /* Create A Stopping Condition */
  }       
  delay_ms(WAIT_FOR_END_SELF_TIMED_WRITE_CYCLE);
  AEEPROMSR|=DATA_READ;    /* Data read flag bit set */
 }
 else{AEEPROMSR=RESET_FLAG; /* Reset all flags */}
}
/* End Program */


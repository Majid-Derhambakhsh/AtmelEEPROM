/**
******************************************************************************
* @file    ATMEL_TWI_EEPROM.h
* @author  Majid Derhambakhsh
* @version V1.6.1
* @date    09-11-2018
* @brief
******************************************************************************
* @description  Set up and use the AT24Cxxxx IC's
*   
* @attention    This file is for Codevision AVR 
                AEEPROMSR [-|-|-|-|DataRead|DataErase|DataWrite|Connected]
                Atmel eeprom Status Register:
                 -DataRead:This flag bit is set when Data is read
                 -DataErase:This flag bit is set when Data is erase
                 -DataWrite:This flag bit is set when Data is write
                 -Connected:This flag bit is set when IC connected 
******************************************************************************
*/

#ifndef _ATMEL_TWI_EEPROM_H
#define _ATMEL_TWI_EEPROM_H
/******************** Librarys Include ********************/ 

#include <stdint.h>

/*************************************** Start Defines ***************************************/

/*---------------------- Define For IC Number ----------------------*/   /* Set By User */

#define IC_NUMBER           AT24C512         /* Set the IC number */
#define IC_NUMBER_FOR_TIME  AT24C512_TIME    /* Set the IC number */

/*-------------------------- Define For WP -------------------------*/   /* Set By User */

#define WP_CONFIG_PORT          DDRC   /* Select DDRx          */
#define WP_PORT                 PORTC  /* Select PORTx         */
#define WP_PIN                  OPIN0  /* Select OPINx         */

/*---------------------- Define For AEEPROMSR ----------------------*/   /* Default Set */

#define CONNECTED    0x01   /* Connected bit */
#define DATA_WRITE   0x02   /* data write bit */
#define DATA_ERASE   0x04   /* data erase bit */
#define DATA_READ    0x08   /* data read bit */
#define RESET_FLAG   0x00   /* Reset all AEEPROMSR bit */

/*--------------------------- Output Pin ---------------------------*/   /* Default Set */

#define OPIN0 0                       /* Select PORTNx.0 */
#define OPIN1 1                       /* Select PORTx.1 */
#define OPIN2 2                       /* Select PORTx.2 */
#define OPIN3 3                       /* Select PORTx.3 */
#define OPIN4 4                       /* Select PORTx.4 */
#define OPIN5 5                       /* Select PORTx.5 */
#define OPIN6 6                       /* Select PORTx.6 */
#define OPIN7 7                       /* Select PORTx.7 */

/*---------------------- Define for IC Setting ---------------------*/   /* Default Set */

/*^^^^^^^^^^^^^^ For Page Byte ^^^^^^^^^^^^^^*/

#define AT24C01     8   /* Set the max number of page */
#define AT24C02     8   /* Set the max number of page */
#define AT24C04     16  /* Set the max number of page */
#define AT24C08     16  /* Set the max number of page */
#define AT24C16     16  /* Set the max number of page */
#define AT24C32     32  /* Set the max number of page */
#define AT24C64     32  /* Set the max number of page */
#define AT24C128    64  /* Set the max number of page */
#define AT24C256    64  /* Set the max number of page */
#define AT24C512    128 /* Set the max number of page */
#define AT24C1024   256 /* Set the max number of page */

/*^^^^^^^^^^^^^^^^ For Time ^^^^^^^^^^^^^^^^^*/

#define AT24C01_TIME     10 /* Set the self timed write cycle */
#define AT24C02_TIME     10 /* Set the self timed write cycle */
#define AT24C04_TIME     10 /* Set the self timed write cycle */
#define AT24C08_TIME     10 /* Set the self timed write cycle */
#define AT24C16_TIME     10 /* Set the self timed write cycle */
#define AT24C32_TIME     10 /* Set the self timed write cycle */
#define AT24C64_TIME     10 /* Set the self timed write cycle */
#define AT24C128_TIME    5  /* Set the self timed write cycle */
#define AT24C256_TIME    5  /* Set the self timed write cycle */
#define AT24C512_TIME    5  /* Set the self timed write cycle */
#define AT24C1024_TIME   5  /* Set the self timed write cycle */

/*----------------------------- Public -----------------------------*/   /* Default Set */

#define ENABLE_GLOBAL_INTERRUPT #asm("sei")                    /* enable global interrupt assembly code */
#define DISABLE_GLOBAL_INTERRUPT #asm("cli")                   /* enable global interrupt assembly code */
#define TRUE 1                                                 /* TRUE */
#define FALSE 0                                                /* FALSE */
#define ERASE 255                                              /* ERASE byte */
#define PAGE_BYTES IC_NUMBER                                   /* Max number of page */
#define NEXT_PAGE_SELECT 1                                     /* Select next page */
#define CORRECT_THE_COUNT 1                                    /* Correct the count */
#define WAIT_FOR_END_SELF_TIMED_WRITE_CYCLE IC_NUMBER_FOR_TIME /* wait for end self timed write cycle */

/**************************************** End Defines ****************************************/

/************************************** Create variable **************************************/

extern uint8_t AEEPROMSR; /* Status register for atmel eeprom */

/************************************** Create Functions *************************************/

void atmel_eeprom_init(void); /* Function for initiation twi */

/*-----------------------------------------------*/

uint8_t atmel_eeprom_connection_test(void); /* Connetction test */

/*-----------------------------------------------*/

void atmel_eeprom_write_byte(uint16_t wb_addres,uint8_t wb_eeprom_data); /* Function For Send Data */

/*-----------------------------------------------*/

void atmel_eeprom_write_nbyte(uint8_t *wnb_str,uint16_t wnb_start_byte,uint16_t wnb_nbyte); /* Function For Send Data */

/*-----------------------------------------------*/

void atmel_eeprom_erase(uint16_t e_start_byte,uint16_t e_end_byte); /* Function For Erase EEPROM */

/*-----------------------------------------------*/

uint8_t atmel_eeprom_read_byte(uint16_t rb_address); /* Function For Read EEPROM */

/*-----------------------------------------------*/

void atmel_eeprom_read_nbyte(uint8_t *rnb_str,uint16_t rnb_start_byte,uint16_t rnb_nbyte); /* Function For Read EEPROM */

/************************************* End Of The Program ************************************/
#endif 